--liquibase formatted sql
--changeset vanmalder:8

CREATE TABLE visits (
    id INT NOT NULL AUTO_INCREMENT,
    petId INT NOT NULL,
    date VARCHAR(45) NOT NULL,
    description VARCHAR(45) NOT NULL,
    PRIMARY KEY (id)
);