--liquibase formatted sql
--changeset vanmalder:7

INSERT INTO pets (ownerId, name, birthdate, type)
VALUES (1, "Kiki", "12-12-2012", "Dog");
INSERT INTO pets (ownerId, name, birthdate, type)
VALUES (1, "Koko", "13-12-2012", "Bird");
INSERT INTO pets (ownerId, name, birthdate, type)
VALUES (2, "Pierre", "14-04-2014", "Whale");