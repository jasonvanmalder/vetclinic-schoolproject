package be.heh.petclinic.component.vet;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import be.heh.petclinic.domain.*;
import java.util.ArrayList;

public class VetImplTest {
 
    @Test
    void compareVetsTest() {
        assertTrue(true);
    }

    boolean compareVets(ArrayList<Vet> v1, ArrayList<Vet> v2) {
        boolean result = true;

        if(v1.size() != v2.size()) result = false;
        else {
            for(int i = 0; i < v1.size(); i++) {
                if(!v1.get(i).getFirstname().equals(v2.get(i).getFirstname())) {
                    result = false;
                }

                if(!v1.get(i).getLastname().equals(v2.get(i).getLastname())) {
                    result = false;
                }

                if(!v1.get(i).getSpecialities().equals(v2.get(i).getSpecialities())) {
                    result = false;
                }
            }
        }

        return result;
    }
}