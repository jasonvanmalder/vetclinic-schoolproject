--liquibase formatted sql
--changeset vanmalder:6

CREATE TABLE pets (
    id INT NOT NULL AUTO_INCREMENT,
    ownerId INT NOT NULL,
    name VARCHAR(45) NOT NULL,
    birthdate VARCHAR(45) NOT NULL,
    type VARCHAR(45) NOT NULL,
    PRIMARY KEY (id) 
);