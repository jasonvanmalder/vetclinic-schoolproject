--liquibase formatted sql
--changeset vanmalder:5

INSERT INTO owners (lastname, firstname, address, city, telephone)
VALUES ("Bon", "Jean", "Rue du jambon 12", "BoucherieVille", "0123 456 1");
INSERT INTO owners (lastname, firstname, address, city, telephone)
VALUES ("Brico", "Juda", "Rue des abricots 21", "FruitVille", "0123 456 2");