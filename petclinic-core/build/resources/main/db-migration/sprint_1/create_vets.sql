--liquibase formatted sql
--changeset vanmalder:2

USE petclinic;

CREATE TABLE vets (
    id INT NOT NULL AUTO_INCREMENT,
    lastname VARCHAR(45) NOT NULL,
    firstname VARCHAR(45) NOT NULL,
    specialities VARCHAR(45) NOT NULL,
    PRIMARY KEY (id) 
);