--liquibase formatted sql
--changeset vanmalder:3

USE petclinic;
INSERT INTO vets (firstname,lastname,specialities) VALUES ('James','Carter','radiology');
INSERT INTO vets (firstname,lastname,specialities) VALUES ('Helen','Leary','surgery');
INSERT INTO vets (firstname,lastname,specialities) VALUES ('Linda','Douglas','radiology');
INSERT INTO vets (firstname,lastname,specialities) VALUES ('Rafael','Ortega','dentistry');