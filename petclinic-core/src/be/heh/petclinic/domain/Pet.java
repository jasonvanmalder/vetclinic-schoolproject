package be.heh.petclinic.domain;

public class Pet {

    private int id;
    private int ownerId;
    private String name;
    private String birthdate;
    private String type;

    public Pet() {}

    public Pet(int id, int ownerId, String name, String birthdate, String type) {
        this.id = id;
        this.ownerId = ownerId;
        this.name = name;
        this.birthdate = birthdate;
        this.type = type;
    }

    public int getId() {
        return this.id;
    }

    public int getOwnerId() {
        return this.ownerId;
    }

    public String getName() {
        return this.name;
    }

    public String getBirthdate() {
        return this.birthdate;
    }

    public String getType() {
        return this.type;
    }

}