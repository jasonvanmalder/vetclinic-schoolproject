package be.heh.petclinic.component.owner;

import java.util.List;
import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import be.heh.petclinic.domain.Owner;

public class OwnerDAO {
    private DataSource dataSource;

    public OwnerDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Owner> getOwners() {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        return template.query("SELECT * FROM owners", new OwnerRowMapper());
    }

    public Owner getOwner(int id) {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        String query = "SELECT * FROM owners WHERE id = ?";
        Owner owner = null;

        try {
            owner = (Owner) template.queryForObject(
                query,
                new Object[] { id },
                new OwnerRowMapper()
            );
        } catch(EmptyResultDataAccessException ignore) {}

        return owner;
    }

    public Owner getOwner(String lastname) {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        String query = "SELECT * FROM owners WHERE lastname = ?";
        Owner owner = null;

        try {
            owner = (Owner) template.queryForObject(
                query, 
                new Object[] { lastname },
                new OwnerRowMapper()
            );
        } catch(EmptyResultDataAccessException ignore) {}

        return owner;
    }

    public boolean addOwner(Owner owner) {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        String query = "INSERT INTO owners(firstname, lastname, address, city, telephone) VALUES(?, ?, ?, ?, ?)";
        
        try {
            template.update(
                query,
                owner.getFirstname(), 
                owner.getLastname(),
                owner.getAddress(),
                owner.getCity(),
                owner.getTelephone()
            );

            return true;
        } catch(Exception e) {
            return false;
        }
    }

    public boolean updateOwner(int id, Owner newOwner) {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        String query = "UPDATE owners SET firstname = ?, lastname = ?, address = ?, city = ?, telephone = ? WHERE id = ?";
        
        try {
            template.update(
                query,
                newOwner.getFirstname(), 
                newOwner.getLastname(),
                newOwner.getAddress(),
                newOwner.getCity(),
                newOwner.getTelephone(),
                id
            );

            return true;
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}