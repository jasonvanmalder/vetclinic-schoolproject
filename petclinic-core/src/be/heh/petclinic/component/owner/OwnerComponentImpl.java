package be.heh.petclinic.component.owner;

import java.util.List;

import javax.sql.DataSource;

import be.heh.petclinic.domain.Owner;

class OwnerComponentImpl implements OwnerComponent {

    private OwnerDAO ownerDAO;

    public OwnerComponentImpl(DataSource dataSource) {
        this.ownerDAO = new OwnerDAO(dataSource);
    }

    @Override
    public List<Owner> getOwners() {
        return this.ownerDAO.getOwners();
    }

    @Override
    public Owner getOwner(int id) {
        return this.ownerDAO.getOwner(id);
    }

    @Override
    public Owner getOwner(String lastname) {
        return this.ownerDAO.getOwner(lastname);
    }

    @Override
    public boolean addOwner(Owner owner) {
        return this.ownerDAO.addOwner(owner);
    }

    @Override
    public boolean updateOwner(int id, Owner newOwner) {
        return this.ownerDAO.updateOwner(id, newOwner);
	}

    @Override
    public boolean checkOwnerExists(Owner owner) {
        if(this.getOwner(owner.getId()) != null && this.getOwner(owner.getLastname()) != null) {
            return true;
        }

		return false;
    }
}