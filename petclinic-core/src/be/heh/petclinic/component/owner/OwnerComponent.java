package be.heh.petclinic.component.owner;

import java.util.List;
import be.heh.petclinic.domain.Owner;

public interface OwnerComponent {
    public List<Owner> getOwners();
    public Owner getOwner(int id);
    public Owner getOwner(String lastname);
    public boolean addOwner(Owner owner);
    public boolean updateOwner(int id, Owner newOwner);
    public boolean checkOwnerExists(Owner owner);
}