package be.heh.petclinic.component.pet;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import be.heh.petclinic.domain.Pet;

public class PetRowMapper implements RowMapper<Pet> {

    @Override
	public Pet mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Pet(
            rs.getInt("id"),
            rs.getInt("ownerId"),
            rs.getString("name"),
            rs.getString("birthdate"),
            rs.getString("type")
        );
	}

}