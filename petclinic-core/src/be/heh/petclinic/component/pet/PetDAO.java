package be.heh.petclinic.component.pet;

import java.util.List;
import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import be.heh.petclinic.domain.Pet;

public class PetDAO {
    private DataSource dataSource;

    public PetDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    public List<Pet> getPets(int ownerId) {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        return template.query(
            "SELECT * FROM pets WHERE ownerId = ?", 
            new Object[] { ownerId }, 
            new PetRowMapper()
        );
    }

    public Pet getPet(int id) {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        String query = "SELECT * FROM pets WHERE id = ?";
        Pet pet = null;

        try {
            pet = (Pet) template.queryForObject(
                query,
                new Object[] { id },
                new PetRowMapper()
            );
        } catch(EmptyResultDataAccessException ignore) {}

        return pet;
    }

    public boolean addPet(Pet pet) {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        String query = "INSERT INTO pets(ownerId, name, birthdate, type) VALUES(?, ?, ?, ?)";
        
        try {
            template.update(
                query,
                pet.getOwnerId(), 
                pet.getName(),
                pet.getBirthdate(),
                pet.getType()
            );

            return true;
        } catch(Exception e) {
            return false;
        }
    }

    public boolean updatePet(int id, Pet newPet) {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        String query = "UPDATE pets SET ownerId = ?, name = ?, birthdate = ?, type = ? WHERE id = ?";
        
        try {
            template.update(
                query,
                newPet.getOwnerId(), 
                newPet.getName(),
                newPet.getBirthdate(),
                newPet.getType(),
                id
            );

            return true;
        } catch(Exception e) {
            return false;
        }
    }
}