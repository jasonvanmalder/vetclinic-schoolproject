package be.heh.petclinic.component.pet;

import java.util.List;

import be.heh.petclinic.domain.Pet;

public interface PetComponent {
    public List<Pet> getPets(int ownerId);
    public Pet getPet(int id);
    public boolean addPet(Pet pet);
    public boolean updatePet(int id, Pet newPet);
}