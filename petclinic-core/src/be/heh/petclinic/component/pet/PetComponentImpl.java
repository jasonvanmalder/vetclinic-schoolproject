package be.heh.petclinic.component.pet;

import java.util.List;
import javax.sql.DataSource;
import be.heh.petclinic.domain.Pet;

class PetComponentImpl implements PetComponent {

    private PetDAO petDAO;

    public PetComponentImpl(DataSource dataSource) {
        this.petDAO = new PetDAO(dataSource);
    }

    @Override
    public List<Pet> getPets(int ownerId) {
        return this.petDAO.getPets(ownerId);
    }

    @Override
    public Pet getPet(int id) {
        return this.petDAO.getPet(id);
    }

    @Override
    public boolean addPet(Pet pet) {
        return this.petDAO.addPet(pet);
    }

    @Override
    public boolean updatePet(int id, Pet newPet) {
        return this.petDAO.updatePet(id, newPet);
	}

}