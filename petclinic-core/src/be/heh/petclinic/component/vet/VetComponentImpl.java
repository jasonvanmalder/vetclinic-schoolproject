package be.heh.petclinic.component.vet;

import java.util.List;

import be.heh.petclinic.domain.Vet;
import javax.sql.DataSource;

class VetComponentImpl implements VetComponent {

    private VetDAO vetDAO;

    public VetComponentImpl(DataSource dataSource) {
        this.vetDAO = new VetDAO(dataSource);
    }

    public List<Vet> getVets() {
        return this.vetDAO.getVets();
    }

}