package be.heh.petclinic.component.vet;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import be.heh.petclinic.domain.Vet;

public class VetDAO {
    private DataSource dataSource;

    public VetDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Vet> getVets() {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        return template.query("SELECT * FROM vets", new VetRowMapper());
    }
}