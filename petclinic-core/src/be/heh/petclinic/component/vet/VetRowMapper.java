package be.heh.petclinic.component.vet;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import be.heh.petclinic.domain.Vet;

public class VetRowMapper implements RowMapper<Vet> {

    @Override
	public Vet mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Vet(
            rs.getString("firstname"),
            rs.getString("lastname"),
            rs.getString("specialities")
        );
	}

}