package be.heh.petclinic.component.visit;

import java.util.List;
import be.heh.petclinic.domain.Visit;

public interface VisitComponent {
    public List<Visit> getVisits(int petId);
    public Visit getVisit(int id);
    public boolean addVisit(Visit visit);
}