package be.heh.petclinic.component.visit;

import java.util.List;

import javax.sql.DataSource;
import be.heh.petclinic.domain.Visit;

class VisitComponentImpl implements VisitComponent {

    private VisitDAO visitDAO;

    public VisitComponentImpl(DataSource dataSource) {
        this.visitDAO = new VisitDAO(dataSource);
    }

    public List<Visit> getVisits(int petId) {
        return this.visitDAO.getVisits(petId);
    }

    @Override
    public Visit getVisit(int id) {
        return this.visitDAO.getVisit(id);
    }

    @Override
    public boolean addVisit(Visit visit) {
        return this.visitDAO.addVisit(visit);
    }

}