package be.heh.petclinic.component.visit;

import java.util.List;
import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import be.heh.petclinic.domain.Visit;

public class VisitDAO {
    private DataSource dataSource;

    public VisitDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    public List<Visit> getVisits(int petId) {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        return template.query(
            "SELECT * FROM visits WHERE petId = ?", 
            new Object[] { petId }, 
            new VisitRowMapper()
        );
    }

    public Visit getVisit(int id) {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        String query = "SELECT * FROM visits WHERE id = ?";
        Visit visit = null;

        try {
            visit = (Visit) template.queryForObject(
                query,
                new Object[] { id },
                new VisitRowMapper()
            );
        } catch(EmptyResultDataAccessException ignore) {}

        return visit;
    }

    public boolean addVisit(Visit visit) {
        JdbcTemplate template = new JdbcTemplate(this.dataSource);
        String query = "INSERT INTO visits(petId, date, description) VALUES(?, ?, ?)";
        
        try {
            template.update(
                query,
                visit.getPetId(), 
                visit.getDate(),
                visit.getDescription()
            );

            return true;
        } catch(Exception e) {
            return false;
        }
    }
}