--liquibase formatted sql
--changeset vanmalder:4

CREATE TABLE owners (
    id INT NOT NULL AUTO_INCREMENT,
    lastname VARCHAR(45) NOT NULL,
    firstname VARCHAR(45) NOT NULL,
    address VARCHAR(45) NOT NULL,
    city VARCHAR(45) NOT NULL,
    telephone VARCHAR(45) NOT NULL,
    PRIMARY KEY (id)
);