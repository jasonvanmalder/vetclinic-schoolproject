package be.jasonvanmalder.petclinicweb;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import be.heh.petclinic.component.owner.OwnerComponent;
import be.heh.petclinic.domain.Owner;

@RestController
public class OwnerController {

    @Autowired
    private OwnerComponent ownerComponent;

    @RequestMapping("/v1/getOwners")
    public ResponseEntity<List<Owner>> getOwners() {
        List<Owner> owners = this.ownerComponent.getOwners();

        if(!owners.isEmpty()) {
            return new ResponseEntity<List<Owner>>(owners, HttpStatus.OK);
        }

        return new ResponseEntity<List<Owner>>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping("/v1/getOwnerById/{id}")
    public ResponseEntity<Owner> getOwnerById(@PathVariable String id) {
        Owner owner = this.ownerComponent.getOwner(Integer.valueOf(id));

        if(owner != null) {
            return new ResponseEntity<Owner>(owner, HttpStatus.OK);
        }

        return new ResponseEntity<Owner>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping("/v1/getOwner/{lastname}")
    public ResponseEntity<Owner> getOwnerByLastname(@PathVariable String lastname) {
        Owner owner = this.ownerComponent.getOwner(lastname);

        if(owner != null) {
            return new ResponseEntity<Owner>(owner, HttpStatus.OK);
        }

        return new ResponseEntity<Owner>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/v1/createOwner", method = RequestMethod.POST)
    public ResponseEntity<String> createOwner(@RequestBody Owner owner) {
        
        if(!this.ownerComponent.checkOwnerExists(owner)) {
            Boolean response = this.ownerComponent.addOwner(owner);
        
            if(response) {
                return new ResponseEntity<String>(HttpStatus.OK);
            }
            
            return new ResponseEntity<String>("Cannot add owner", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<String>("Owner already exists", HttpStatus.NOT_MODIFIED);
        
    }

    @RequestMapping(value = "/v1/editOwner/{id}", method = RequestMethod.PUT)
    public ResponseEntity<String> editOwner(@PathVariable String id, @RequestBody Owner owner) {
        
        int ownerId = Integer.valueOf(id);

        if(this.ownerComponent.getOwner(ownerId) != null) {
            Boolean response = this.ownerComponent.updateOwner(ownerId, owner);
        
            if(response) {
                return new ResponseEntity<String>(HttpStatus.OK);
            }
            
            return new ResponseEntity<String>("Cannot edit owner", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<String>("Owner doesn't exist", HttpStatus.NOT_MODIFIED);
    }
    
}