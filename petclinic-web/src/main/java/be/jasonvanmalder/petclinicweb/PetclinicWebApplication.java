package be.jasonvanmalder.petclinicweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"classpath*:be/heh/petclinic/config.xml","classpath*:be/heh/petclinic/**/component.xml"})
public class PetclinicWebApplication {
	public static void main(String[] args) {
		SpringApplication.run(PetclinicWebApplication.class, args);
	}
}
