package be.jasonvanmalder.petclinicweb;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import be.heh.petclinic.component.visit.VisitComponent;
import be.heh.petclinic.domain.Visit;

@RestController
public class VisitController {

    @Autowired
    private VisitComponent visitComponent;

    @RequestMapping("/v1/getVisits/{pet}")
    public ResponseEntity<List<Visit>> getVisits(@PathVariable String pet) {
        List<Visit> visits = this.visitComponent.getVisits(Integer.valueOf(pet));

        if(!visits.isEmpty()) {
            return new ResponseEntity<List<Visit>>(visits, HttpStatus.OK);
        }

        return new ResponseEntity<List<Visit>>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/v1/createVisit", method = RequestMethod.POST)
    public ResponseEntity<String> createOwner(@RequestBody Visit visit) {
        Boolean response = this.visitComponent.addVisit(visit);
    
        if(response) {
            return new ResponseEntity<String>(HttpStatus.OK);
        }
        
        return new ResponseEntity<String>("Cannot add visit", HttpStatus.INTERNAL_SERVER_ERROR);        
    }
    
}