package be.jasonvanmalder.petclinicweb;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import be.heh.petclinic.component.pet.PetComponent;
import be.heh.petclinic.domain.Pet;

@RestController
public class PetController {

    @Autowired
    private PetComponent petComponent;

    @RequestMapping("/v1/getPets/{owner}")
    public ResponseEntity<List<Pet>> getPets(@PathVariable String owner) {
        List<Pet> pets = this.petComponent.getPets(Integer.valueOf(owner));
        
        if(!pets.isEmpty()) {
            return new ResponseEntity<List<Pet>>(pets, HttpStatus.OK);
        }

        return new ResponseEntity<List<Pet>>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/v1/createPet", method = RequestMethod.POST)
    public ResponseEntity<String> createOwner(@RequestBody Pet pet) {
        Boolean response = this.petComponent.addPet(pet);
    
        if(response) {
            return new ResponseEntity<String>(HttpStatus.OK);
        }
        
        return new ResponseEntity<String>("Cannot add pet", HttpStatus.INTERNAL_SERVER_ERROR);        
    }

    @RequestMapping("/v1/getPet/{id}")
    public ResponseEntity<Pet> getPet(@PathVariable String id) {
        Pet pet = this.petComponent.getPet(Integer.valueOf(id));

        if(pet != null) {
            return new ResponseEntity<Pet>(pet, HttpStatus.OK);
        }

        return new ResponseEntity<Pet>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/v1/editPet/{id}", method = RequestMethod.PUT)
    public ResponseEntity<String> editPet(@PathVariable String id, @RequestBody Pet pet) {
        int petId = Integer.valueOf(id);

        if(this.petComponent.getPet(petId) != null) {
            Boolean response = this.petComponent.updatePet(petId, pet);
        
            if(response) {
                return new ResponseEntity<String>(HttpStatus.OK);
            }
            
            return new ResponseEntity<String>("Cannot edit pet", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<String>("Pet doesn't exist", HttpStatus.NOT_MODIFIED);
    }
    
}